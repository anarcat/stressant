stressant (0.7.0) unstable; urgency=medium

  [ Antoine Beaupré ]
  * documentation updates:
    * add ars fio commands
    * mention nvme drives
    * move README in the right place
    * move similar software to design
    * split related projects in groups
    * add tinymembench
    * show how we actually run a basic bench
    * checkbox is another alternative to stressant
  * new features:
    * make a basic fio test bench, only shipped in source
    * fix syntax error in docs
    * start listing iperf server lists
    * add fqdn to email subject and default logfile
    * add timestamps to email and logs
    * NegateAction: allow caller to override default
  * fixes:
    * reverse logic of default NegateAction argument
    * properly flush changes to disk before calling fio job
  * code cleanups:
    * fix branch name in gbp
    * lint: fix long line wrap

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Use canonical URL in Vcs-Git.
  * Update standards version to 4.5.1, no changes needed.

 -- Antoine Beaupré <anarcat@debian.org>  Thu, 09 Feb 2023 11:37:48 -0500

stressant (0.6.0) unstable; urgency=medium

  * documentation updates:
    * mention disk-filltest
    * do not verify disk wipes and use the "random" method
    * "stressant.py" was renamed to "stressant"
    * factor in NMU from plugwash, thanks!
  * code changes:
    * remove bare except, assuming we mean to catch other SMTP exceptions
    * cosmetic: harmonize fio argument order between file and dir
    * cosmetic: remove cargo-culted comments referring to previous code
  * API-breaking changes:
    * merge the two disk sizes arguments (--diskPercent and --fileSize
      merge into --writeSize)
    * deprecate the ISO build functionality, we made it into grml
  * new features:
    * cap disk tests to one minute by default (--diskRuntime)
    * bump Standards-Version to 4.5.0, no change
    * improve error messages in SMTP backoff mechanism
    * backport Python 3.7 improvements to SMTP buffering handler
  * bugfixes:
    * fix unicode decode error in Python 3.7
    * create directory earlier, so that dd can work
    * fix job file argument so it supports jobs with filename=
    * fix error handling in SMTP handler
    * fix infinite loop in SMTP handler

 -- Antoine Beaupré <anarcat@debian.org>  Thu, 20 Feb 2020 09:43:58 -0500

stressant (0.5.0+nmu1) unstable; urgency=medium

  * Non-maintainer upload.
  * Re-upload source-only to allow migration to testing (Closes: 943867).
  * Fix clean target.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 09 Nov 2019 07:50:54 +0000

stressant (0.5.0) unstable; urgency=medium

  * ship new stressant-meta package
  * add mkosi to the list of iso generators
  * update and clarify live build images section
  * add more detailed iperf tests
  * add warning and example of fake card and repair
  * tell user how to see smart results
  * run smart tests after other disk benchmarks
  * use job files instead of static fio configuration
  * rename jobfile argument to abstract away fio
  * link to debian manpages
  * use proper smartmontools dependency (Closes: #859698)
  * add more related projects
  * mention more simple disk tests
  * mention that wiping disks is a fairly good way to test them
  * more generic title for flash memory
  * include s-tui in meta
  * link to the bench.sh scripts
  * port to python3 (Closes: #938573)
  * fix --version usage
  * some linting

 -- Antoine Beaupré <anarcat@debian.org>  Sun, 27 Oct 2019 16:29:43 -0400

stressant (0.4.1) unstable; urgency=medium

  * remove traces of monkeysign
  * hide serial numbers from smartctl output
  * python packages are arch:all, not any
  * split docs in a separate package

 -- Antoine Beaupré <anarcat@debian.org>  Tue, 27 Jun 2017 22:59:36 -0400

stressant (0.4.0) unstable; urgency=medium

  * can now test in arbitrary directories with --directory option
  * fix "edit" links in docs
  * fix version parsing
  * fix version detection in sphinx build
  * fix sphinx build-dep
  * add examples to manpage
  * explain how the usage and manpages are generated
  * deal with older colorlog modules
  * fix changelog generation with gbp

 -- Antoine Beaupré <anarcat@debian.org>  Sun, 23 Apr 2017 20:03:41 -0400

stressant (0.3.2) unstable; urgency=medium

  * hook grml build into Gitlab CI
  * various documentation updates, including:
    * review builders section
    * credit grml in acknowledgements
    * add asciinema screncast
    * convert documentation to Sphinx/RST
    * add manpage

 -- Antoine Beaupré <anarcat@debian.org>  Sun, 02 Apr 2017 21:50:04 -0400

stressant (0.3.1) unstable; urgency=medium

  * fix copyright file

 -- Antoine Beaupré <anarcat@debian.org>  Fri, 17 Mar 2017 12:02:16 -0400

stressant (0.3.0) unstable; urgency=medium

  * rebuild the whole project using Grml
  * rewrite and complete prototype in Python
  * email, logfile and color support
  * network tests with iPerf3
  * disk tests with fio
  * CPU tests with stress-ng
  * basic information gathered with lshw and smart

 -- Antoine Beaupré <anarcat@debian.org>  Fri, 17 Mar 2017 11:10:21 -0400

stressant (0.2) experimental; urgency=medium

  * switch to using vmdebootstrap to build ISO images
  * add first prototype of test run, not packaged
  * switch to native packaging
  * Initial release (Closes: #707178)

 -- Antoine Beaupré <anarcat@debian.org>  Thu, 05 Jan 2017 02:50:03 -0500
