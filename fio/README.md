# Basic disk I/O tests

Those tests are based on the [fio test suite](https://github.com/axboe/fio/), and the actual
specifications are inspired by [Jim Salter's](http://www.jrs-s.net/) [scaffolding](https://github.com/jimsalterjrs/fio-test-scaffolding/) and
[his article about fio](https://arstechnica.com/gadgets/2020/02/how-fast-are-your-disks-find-out-the-open-source-way-with-fio/).

This could, ideally, all be merged in a single fio job
file. Unfortunately, the metrics output by fio are a little clobbered
when running tasks serially (with `stonewall=1`), see [this
discussion](https://lore.kernel.org/fio/87pmkaeicg.fsf@curie.anarc.at/) for how that works and a possible patch idea.

Running the tests should be basically:

    for test in *.fio; do
        fio --output-format=normal,terse $test | tee -a job.log
        sleep 1
    done

Then the results are dumped in `job.log`. To parse that log
automatically, use something like this, as a markdown table:

    printf "| test | error | read I/O | read IOPS | write I/O | write IOPS |\n"
    grep '^3;' job.log | sort | cut -d';' -f3,6,7,8,48,49 | sed 's/;/ | /g;s/^/| /;s/$/ |/'

This would look, for example, like this when aligned:

| test                   | error | read I/O | read IOPS | write I/O | write IOPS |
|------------------------|-------|----------|-----------|-----------|------------|
| randread-4k-4g-1x      | 0     | 74469    | 18617     | 0         | 0          |
| randread-4k-4g-1x      | 0     | 75953    | 18988     | 0         | 0          |
| randread-64k-256m-16x  | 0     | 1809133  | 28267     | 0         | 0          |
| randread-64k-256m-16x  | 0     | 1812978  | 28327     | 0         | 0          |
| randwrite-4k-4g-1x     | 0     | 0        | 0         | 25453     | 6363       |
| randwrite-4k-4g-1x     | 0     | 0        | 0         | 25552     | 6388       |
| randwrite-64k-256m-16x | 0     | 0        | 0         | 66626     | 1041       |

This is a simpler version of the parsing Salter does in his [wrapper
script](https://github.com/jimsalterjrs/fio-test-scaffolding/blob/fa284a26c290d40ac220c7640656adeee3609e69/fio-test.pl#L61-L88). It is also available as [my own wrapper script](https://gitlab.com/anarcat/scripts/-/blob/main/fio-ars-bench.sh), which
should be merged with the main stressant Python script, see [issue
5](https://gitlab.com/anarcat/stressant/-/issues/5) for a discussion of that.

Salter also does:

    sync
    echo 3 > /proc/sys/vm/drop_caches

... but this is what `fio --invalidate` does, and it's the default.

Also note the interesting [fio-plot](https://github.com/louwrentius/fio-plot) projects which draws fancy
graphs based on fio jobs. It varies queue depth and number of jobs, by
default, interestingly.
