#!/usr/bin/python
# -*- coding: utf-8 -*-

from setuptools import setup

__description__ = '''Stressant is a simple yet complete stress-testing tool that forces
a computer to perform a series of test using well-known Linux software
in order to detect possible design or construction failures.'''
__website__ = 'https://gitlab.com/anarcat/stressant'
__author__ = 'Antoine Beaupré'
__author_email__ = 'anarcat@debian.org'
__license__ = 'AGPLv3'
__copyright__ = '''
Copyright (C) 2017 Antoine Beaupré <anarcat@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

if __name__ == '__main__':
    setup(name='stressant',
          description='Simple stress-test tool',
          long_description=__description__,
          url=__website__,
          author=__author__,
          author_email=__author_email__,
          license=__license__,
          install_requires=['humanize', 'colorlog'],
          setup_requires=['sphinx', 'setuptools_scm'],
          use_scm_version={
              'write_to': '__version.py',
          },
          packages=[],
          scripts=['stressant'],
          classifiers=[
              'Development Status :: 4 - Beta',
              'Intended Audience :: Developers',
              'Intended Audience :: System Administrators',
              'Intended Audience :: End Users/Desktop',
              'Intended Audience :: Information Technology',
              'License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)',
              'Programming Language :: Python',
              'Programming Language :: Python :: 2',
              'Programming Language :: Python :: 2.7',
              'Programming Language :: Python :: 3',
              'Programming Language :: Python :: 3.7',
              'Operating System :: POSIX :: Linux',
              'Environment :: Console',
              'Natural Language :: English',
              'Topic :: System :: Hardware',
              'Topic :: System :: Recovery Tools',
              'Topic :: System :: Systems Administration'
              ])
