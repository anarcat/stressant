Stressant - stress testing tool
===============================

`Stressant <https://gitlab.com/anarcat/stressant>`__ is a simple yet
complete stress-testing tool that forces a computer to perform a series
of test using well-known Linux software in order to detect possible
design or construction failures.

Stressant builds on top of existing software and tries to cover most
components of you system (currently disk, CPU and processor). It is
part of the `Grml Live Linux <https://grml.org/>`_ project and also
packaged for Debian.

Features
========

 * disk testing (with :manpage:`smartctl(8)` and :manpage:`fio()`)
 * CPU testing (with :manpage:`stress-ng(1)`)
 * network testing (with :manpage:`iperf3(1)`)
 * designed to be ran automatically
 * supports sending reports by email or saving to disk
 * basic benchmarks (with :manpage:`hdparm(8)` and :manpage:`dd(1)`)

See also the :doc:`design` document for other planned features and
remaining work.

Installation instructions are in the :doc:`install` document.

Contributions are welcome, see the :doc:`contribute` document.

Demo
====

.. see the URL below for an online video demo

|asciicast|

.. |asciicast| image:: https://asciinema.org/a/107950.png
   :target: https://asciinema.org/a/107950

Acknowledgements
================

Thanks to jrollins & dkg for writing debirf which was so useful in
creating the first Stressant prototypes, and especially to dkg for help
with qemu and other oddities. Thanks also to taggart for spurring this
idea forward. Finally, thanks to the Grml team for being providing
useful feedback on the toolset and for welcoming Stressant contributions
in Grml itself.
