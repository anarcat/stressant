.. include:: ../README.rst

.. toctree::
   :hidden:
   :maxdepth: 2

   install
   Usage <usage>
   design
   contribute
   history
