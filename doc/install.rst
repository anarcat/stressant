Installation
============

Stressant is a Python program that can be installed in the usual way
`from PyPI <https://pypi.python.org/pypi/stressant>`__::

    pip install stressant

It can also be installed `from
source <https://gitlab.com/anarcat/stressant>`__::

    ./setup.py install

If you are running Debian "buster" (10) or later, you can also install
the Debian package::

    apt install stressant

Downloading images
~~~~~~~~~~~~~~~~~~

Stressant is also designed to boot from a live CD or USB stick. It can
also boot from the network with a proper configuration.

Pre-built images are available in the `Grml Linux Linux distribution
<https://grml.org/download/>`_, in the "FULL" images. In particular,
see the `daily images <https://grml.org/daily/>`_ for the latest.

Installing the image
~~~~~~~~~~~~~~~~~~~~

You can deploy the ISO on a USB stick with the following command on most
UNIX systems, where ``/dev/sdX`` is the device for your USB key::

    sudo cp grml96-full_sid_latest.iso /dev/sdX

To get better progress information, you can use the fantastic ``pv``
package::

    pv grml96-full_sid_latest.iso | sudo dd of=/dev/sdX conv=fdatasync

For other operating systems, you may try those different options:

-  For *Debian*: see the `official Debian
   documentation <http://www.debian.org/releases/stable/amd64/ch04s03.html#usb-copy-isohybrid>`__
-  For *Ubuntu*: try the `Ubuntu Startup Disk
   Creator <https://www.ubuntu.com/download/desktop/create-a-usb-stick-on-ubuntu>`__
-  For *Fedora* and derivatives: try the `Media
   Writer <https://github.com/MartinBriza/MediaWriter>`__ or follow
   `those
   instructions <https://fedoraproject.org/wiki/How_to_create_and_use_Live_USB>`__
-  For *Microsoft Windows*: use the
   `win32diskimager <https://sourceforge.net/projects/win32diskimager/>`__
   software
-  For all platforms: maybe also try `Etcher <https://etcher.io/>`__ if
   the above is too complicated

    Note: it seems that Mac OS is constantly changing how this works.
    There used to be support for burning ISO images directly in the Disk
    Utility, but that feature was removed. You may want to try `this
    approach <http://apple.stackexchange.com/a/100458>`__ if Etcher
    doesn't work for you.

    Note: this should be moved in the main Grml documentation.

Booting images using PXE
~~~~~~~~~~~~~~~~~~~~~~~~

Inside the ISO file, Grml is build as a
`squashfs <https://en.wikipedia.org/wiki/SquashFS>`__ and one can use
the ``fetch=URL`` argument to fetch that squashfs over the network.
Those `german
instructions <http://www.pro-linux.de/kurztipps/2/1432/grml-small-200811-via-pxe-booten-ohne-nfs.html>`__
show how to extract the squashfs from the image and set it up in a PXE
menu, but basically, what you need is::

    kernel grml/2008.11/small-linux26
    append initrd=grml/2008.11/small-minirt26.gz boot=live fetch=http://grml.example.com/grml/grml-small.squashfs

The ``small-*`` kernel and initrd come from the
`grml-terminalserver <http://grml.org/terminalserver/>`__ project.

Netbooting images is very useful in all sorts of situations. While
Grml's `grml-terminalserver <http://grml.org/terminalserver/>`__ tool
helps in automatically configure a PXE server, you may want to configure
your own, more complete setup. See for now the `Koumbit
wiki <https://wiki.koumbit.net/PxeMaintenance>`__ for PXE configuration
documentation.

    I have done xperimentes to `boot ISO images over the
    network <http://superuser.com/questions/304722/is-it-possible-to-boot-from-an-iso-over-the-network-and-how>`__
    but those have been generally unsuccessful.

    Note: this should be moved in the main Grml documentation.

    Also note that you can directly boot into Grml and/or Stressant
    using a tool like `netboot.xyz <https://netboot.xyz/>`__ and
    `iPXE <http://ipxe.org/>`__.

Testing images in qemu
~~~~~~~~~~~~~~~~~~~~~~

A good way to test Stressant without having to reboot your computer is
with an emulator like `Qemu <http://qemu.org/>`__. This will load the 64
bit image in a KVM-enabled 512 MB image running only on the serial
console::

    qemu-system-x86_64 -m 512 -enable-kvm \
        -serial mon:stdio -display none \
        grml96-full-stressant.iso

See also the `Qemu cheat sheet in the Koumbit
wiki <https://wiki.koumbit.net/Qemu>`__.

